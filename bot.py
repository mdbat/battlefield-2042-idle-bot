import random

import keyinput, time, win32gui, win32api, win32con, win32process, cv2, sys, ctypes
from PIL import ImageGrab
import numpy as np
import os, signal, threading
import re
import datetime


# Exit on interrupt (this is a multithreaded program so do it ourselves)
def interrupt_handler(signum, frame):
    os._exit(1) # Kinda hacky but I couldnt care less about a clean exit


signal.signal(signal.SIGINT, interrupt_handler)


# Position of the battlefield legends window
window_x = -1
window_y = -1
window_width = -1
window_height = -1

# Windows process related stuff
window_pid = -1
window_path = ""
window_found = False

# Whether this is targetting the steam version of the game,
# known through the use of the "steam" launch options
isSteamVersion = False

# Launch options parsed as arguments in the "optionname" or "optioname=value" format
launch_options = {}


def log(msg: str):
    now = datetime.datetime.now()
    print(f'[{now.hour:{"0"}{2}}:{now.minute:{"0"}{2}}:{now.second:{"0"}{2}}] {msg}')


# Routine to kill a process, used when it gets frozen permanently
def killprocess(pid):
    try:
        os.kill(pid, signal.SIGTERM)
    except:
        print('Failed to kill the process, will try again later.')


# Callback for windows enumeration
def getbattlefieldwindow(hwnd, extra):
    if win32gui.GetWindowText(hwnd) != "Battlefield™ 2042":
        return

    global window_found
    window_found = True

    global window_pid
    global window_path
    thread_id, window_pid = win32process.GetWindowThreadProcessId(hwnd)

    handle = win32api.OpenProcess(win32con.PROCESS_QUERY_INFORMATION | win32con.PROCESS_VM_READ, False, window_pid)
    window_path = win32process.GetModuleFileNameEx(handle, 0)

    rect = win32gui.GetClientRect(hwnd)
    rect_with_borders = win32gui.GetWindowRect(hwnd)
    window_pos = win32gui.ClientToScreen(hwnd, (0, 0))
    global window_x
    global window_y
    global window_width
    global window_height
    window_x = window_pos[0]
    window_y = window_pos[1]
    window_width = rect[2]
    window_height = rect[3]

    # Get the size of borders
    window_width_with_borders = rect_with_borders[2] - rect_with_borders[0]
    window_height_with_borders = rect_with_borders[3] - rect_with_borders[1]

    total_border_width = window_width_with_borders - window_width
    total_border_height = window_height_with_borders - window_height
    # Horizontally, the border only comprises the snapping border, which wraps the sides (and also the bottom)
    snapping_border_width = total_border_width / 2
    # But not the top of the top bar !
    top_bar_height = total_border_height - snapping_border_width

    # Check for whether the window has the right resolution
    if window_width != 1280 or window_height != 720:
        # If not, resize the window. Take into account the fact we are mixing coordinates with no borders
        # and coordinates with borders
        win32gui.SetWindowPos(hwnd,
                              0,
                              int(window_x - snapping_border_width),
                              int(window_y - top_bar_height),
                              int(1280 + total_border_width),
                              int(720 + total_border_height),
                              0)
        window_width = 1280
        window_width = 720


# Keys to press to move around
keys = [keyinput.W, keyinput.D, keyinput.S, keyinput.A]

# When moving around, how long in seconds since the last keypress.
last_keypress_timestamp = time.time()

# Start with a 'random' duration of activity 30 seconds
random_duration = 30


# Function to move around ingame
def move_around():
    current_time = time.time()

    global last_keypress_timestamp
    global random_duration

    if (current_time - last_keypress_timestamp) > random_duration:
        rand_key_index = random.randint(0, 3)

        keyinput.holdKey(keys[rand_key_index], 0.1)

        last_keypress_timestamp = time.time()

        # Re-generate a random number between 120 and 180. If the amount of seconds since last keypress is greater,
        # We press a random key for a tenth of a second.
        random_duration = random.uniform(120.0, 180.0)


# Function to press ESC
def press_esc(x, y):
    keyinput.pressKey(keyinput.ESC)
    time.sleep(0.05)
    keyinput.releaseKey(keyinput.ESC)


# Function to press space
def press_space(x, y):
    keyinput.pressKey(keyinput.SPACE)
    time.sleep(0.05)
    keyinput.releaseKey(keyinput.SPACE)


# Function to kill the game when an engine error pops up
def kill_game_for_error(x, y):
    log('Killing the game after having detected an error')
    killprocess(window_pid)


def hold_ctrl(x, y):
    keyinput.holdKey(keyinput.CTRL, 2.0)


def press_e_and_aim_down(x, y):
    keyinput.pressKey(keyinput.E)
    time.sleep(0.05)
    keyinput.releaseKey(keyinput.E)

    # Aim down for about three seconds
    for i in range(300):
        # This is window-specific
        keyinput.move_directinput(0, 300)
        time.sleep(0.01)


# Find a certain item in an image, returns the estimated position and associated threshold
def find_item(img, template):
    # Apply template Matching
    res = cv2.matchTemplate(img, template, cv2.TM_CCOEFF_NORMED)

    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

    w, h = template.shape[::-1]

    # Top left of the area
    top_left = max_loc
    # Buttom right of the area
    bottom_right = (top_left[0] + w, top_left[1] + h)

    # Zone to click if we do need
    middle_x = (top_left[0] + bottom_right[0]) / 2
    middle_y = (top_left[1] + bottom_right[1]) / 2

    return {'threshold': max_val, 'x': middle_x, 'y': middle_y, 'raw_result': res}


# Take a screenshot of the window
def screenshot(x, y, width, height):
    image = ImageGrab.grab(bbox=(x, y, x + width, y + height))
    image = cv2.cvtColor(np.array(image), cv2.COLOR_RGB2BGR)
    return image


# The previous image used in the watchdog
old_img = None


# Watchdog running on another thread, makes sure the battlefield process didn't crash or is not stuck
def process_watchdog():
    while True:
        # Check for the process being alive every 10 minutes
        time.sleep(600)

        global window_pid
        global window_path
        global window_found

        # Try to find the window handle
        hwnd = win32gui.FindWindow(None, "Battlefield™ 2042")

        # Make sure the Window is still alive
        if hwnd == 0:
            window_found = False
            # Restart the process if the window died
            log('Game is not on, re-starting it...')

            # If this is the Origin version, we can directly use the executable itself
            if not isSteamVersion:
                os.system('"' + window_path + '"')
            # If this is the Steam version, use the steam URL or the game won't be able to connect online
            else:
                os.system("start \"\" steam://rungameid/1517290")

            continue

        # Refresh the window's position
        win32gui.EnumWindows(getbattlefieldwindow, None)

        # Put it in foreground
        try:
            win32gui.SetForegroundWindow(hwnd)
        except:
            print('Failed to put the window in foreground')

        global old_img

        # Capture the window in color
        color_img = screenshot(window_x, window_y, window_width, window_height)

        # Convert it to grayscale for faster processing
        img = cv2.cvtColor(color_img, cv2.COLOR_BGR2GRAY)

        if old_img is not None:
            # Compare the current image to the old one,
            # if it didn't change within 60 seconds, the process is likely stuck
            result = find_item(img, old_img)

            # We deem the image as being the same if it matches over 99.999%
            if result['threshold'] > 0.99999:
                log('Process appears to be frozen, killing it for a later restart')
                # Kill the process if it matches
                killprocess(window_pid)

        # Save the current image to compare it to the next one being read
        old_img = img


# Templates that we are looking for in the image
deploy_button = cv2.imread("Images/deploy_button.png", cv2.IMREAD_GRAYSCALE)
deploy_squad_button = cv2.imread("Images/deploy_squad_button.png", cv2.IMREAD_GRAYSCALE)
ready_button = cv2.imread("Images/ready_button.png", cv2.IMREAD_GRAYSCALE)
exit_to_menu_button = cv2.imread("Images/exit_to_menu_button.png", cv2.IMREAD_GRAYSCALE)
exit_to_menu_button_hovered = cv2.imread("Images/exit_to_menu_button_hovered.png", cv2.IMREAD_GRAYSCALE)
exit_to_menu_confirm_button = cv2.imread("Images/exit_to_menu_confirm_button.png", cv2.IMREAD_GRAYSCALE)
ru_button = cv2.imread("Images/ru_button.png", cv2.IMREAD_GRAYSCALE)
us_button = cv2.imread("Images/us_button.png", cv2.IMREAD_GRAYSCALE)
close_button = cv2.imread("Images/close_button.png", cv2.IMREAD_GRAYSCALE)
close_button_hovered = cv2.imread("Images/close_button_hovered.png", cv2.IMREAD_GRAYSCALE)
retry_button = cv2.imread("Images/retry_button.png", cv2.IMREAD_GRAYSCALE)
retry_button_hovered = cv2.imread("Images/retry_button_hovered.png", cv2.IMREAD_GRAYSCALE)

# All the different game modes
breakthrough_button = cv2.imread("Images/breakthrough_button.png", cv2.IMREAD_GRAYSCALE)
breakthrough_button_hovered = cv2.imread("Images/breakthrough_button_hovered.png", cv2.IMREAD_GRAYSCALE)
standard_breakthrough_button = cv2.imread("Images/standard_breakthrough_button.png", cv2.IMREAD_GRAYSCALE)
standard_breakthrough_button_hovered = cv2.imread("Images/standard_breakthrough_button_hovered.png", cv2.IMREAD_GRAYSCALE)

conquest_button = cv2.imread("Images/conquest_button.png", cv2.IMREAD_GRAYSCALE)
conquest_button_hovered = cv2.imread("Images/conquest_button_hovered.png", cv2.IMREAD_GRAYSCALE)
standard_conquest_button = cv2.imread("Images/standard_conquest_button.png", cv2.IMREAD_GRAYSCALE)
standard_conquest_button_hovered = cv2.imread("Images/standard_conquest_button_hovered.png", cv2.IMREAD_GRAYSCALE)

# UI_elements = [{'image': exit_confirm_button, 'threshold': 0.95, 'callback': press_space}]
UI_elements = [{'image': deploy_button, 'threshold': 0.95, 'callback': keyinput.click},
               {'image': deploy_squad_button, 'threshold': 0.95, 'callback': keyinput.click},
               {'image': ready_button, 'threshold': 0.95, 'callback': keyinput.click},
               {'image': ru_button, 'threshold': 0.95, 'callback': keyinput.click},
               {'image': us_button, 'threshold': 0.95, 'callback': keyinput.click},
               {'image': exit_to_menu_confirm_button, 'threshold': 0.95, 'callback': keyinput.click},
               {'image': exit_to_menu_button, 'threshold': 0.90, 'callback': keyinput.click},
               {'image': exit_to_menu_button_hovered, 'threshold': 0.90, 'callback': keyinput.click},
               {'image': retry_button, 'threshold': 0.90, 'callback': keyinput.click},
               {'image': retry_button_hovered, 'threshold': 0.95, 'callback': keyinput.click},
               {'image': close_button, 'threshold': 0.95, 'callback': keyinput.click},
               {'image': close_button_hovered, 'threshold': 0.95, 'callback': keyinput.click}]

# Parse launch options if any
if len(sys.argv) > 1:
    # Regex used to parse options
    arg_regex_template = '(.*)=(.*)'
    arg_regex = re.compile(arg_regex_template)

    for argv in sys.argv[1:]:
        match = arg_regex.match(argv)
        if match is not None:
            launch_options[match.group(1)] = match.group(2)
        else:
            launch_options[argv] = None

if 'steam' in launch_options:
    log('Steam version specified')
    isSteamVersion = True


if 'mode' not in launch_options:
    launch_options['mode'] = 'conquest'


if launch_options['mode'] == 'breakthrough':
    UI_elements.append({'image': standard_breakthrough_button, 'threshold': 0.95, 'callback': keyinput.click})
    UI_elements.append({'image': standard_breakthrough_button_hovered, 'threshold': 0.95, 'callback': keyinput.click})
    UI_elements.append({'image': breakthrough_button, 'threshold': 0.85, 'callback': keyinput.click})
    UI_elements.append({'image': breakthrough_button_hovered, 'threshold': 0.95, 'callback': keyinput.click})

if launch_options['mode'] == 'conquest':
    UI_elements.append({'image': standard_conquest_button, 'threshold': 0.95, 'callback': keyinput.click})
    UI_elements.append({'image': standard_conquest_button_hovered, 'threshold': 0.95, 'callback': keyinput.click})
    UI_elements.append({'image': conquest_button, 'threshold': 0.85, 'callback': keyinput.click})
    UI_elements.append({'image': conquest_button_hovered, 'threshold': 0.95, 'callback': keyinput.click})


# Set ourselves as DPI aware, or else we won't get proper pixel coordinates if scaling is not 100%
errorCode = ctypes.windll.shcore.SetProcessDpiAwareness(2)


log("Battlefield bot starting in 5 seconds, bring battlefield window in focus...")
time.sleep(5)

win32gui.EnumWindows(getbattlefieldwindow, None)

# If the window hasn't been found, exit
if not window_found:
    log("No Battlefield Legends window found")
    sys.exit(1)

# Start the watchdog thread
watchdogthread = threading.Thread(target=process_watchdog)
watchdogthread.start()

while True:
    # If the window died, stop running analysis for a moment */
    if window_found is False:
        time.sleep(5)
        continue

    # Capture the window in color
    color_img = screenshot(window_x, window_y, window_width, window_height)
    # Convert it to grayscale for faster processing
    img = cv2.cvtColor(color_img, cv2.COLOR_BGR2GRAY)

    found_UI_element = False

    # Try to find any of the templates
    for element in UI_elements:
        result = find_item(img, element['image'])

        if result['threshold'] > element['threshold']:
            found_UI_element = True

            # Click all elements that potentially match
            if 'multiple' in element:
                num_of_points = int(element['multiple'])
                raw_result = result['raw_result']

                # Find the highest matching patterns
                coords_flattened = np.argpartition(raw_result.flatten(), -num_of_points)[-num_of_points:]
                coords_2d_yx = np.unravel_index(coords_flattened, raw_result.shape)

                coords = []

                w, h = element['image'].shape[::-1]

                for y, x in zip(coords_2d_yx[0], coords_2d_yx[1]):
                    coords.append((x + w / 2, y + h / 2))

                # Click them all with a delay of half a second inbetween
                for coord in coords:
                    element['callback'](window_x + int(coord[0]), window_y + int(coord[1]))
                    time.sleep(0.5)
            else:
                element['callback'](window_x + int(result['x']), window_y + int(result['y']))
            break

    # If no UI element got found, move around is the default behavior
    if found_UI_element is False:
        move_around()

    # Sleep for a second
    time.sleep(1.0)
