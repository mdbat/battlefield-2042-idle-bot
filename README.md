# Apex Idle bot
This program is designed for farming XP on Battlefield 2042 by continuously queuing 
for matches and moving around in game to avoid kicks due to inactivity.  
The bot accounts for any UI prompt, error message, etc. and can also select a
specific character on the character selection screen.

## Setup
* Install Python 3 from https://www.python.org/downloads/. Check the option
"Add to PATH" to be able to launch python from the commandline.
* Install the packages required by the program using PIP with the command
"pip install ..." (or "pip3 install..."). The required packages are:
    * pypiwin32 : for access to the Windows API (windowing, input, ...).
    * pillow : used to take screenshots of the desktop.
    * opencv-python : used for image analysis.
* Download this program (duh !).

## How to run
* Start Battlefield 2042 in windowed mode, and simply run the program 
using the command "python bot.py" (or "python3 bot.py"). The bot should start its
keypresses 5 seconds after launch.  
* Launch options: You can specify launch options to the bot. They are either in the
format 'name=value' (e.g. oneplusone=two) or simply 'name' for options that are just
an on-off switch.
Available launch options are:
    * 'mode=XXX'. If you want the bot to select a particular mode, add the name of the mode you desire
    as a parameter. Can either be 'conquest' or 'breakthrough'.
    * 'steam'. Specifies the game was launched via steam, uses the steam command to start the game.